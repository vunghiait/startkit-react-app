export interface Config {
	BASENAME?: string
	TOKEN_KEY: string
	SUCCESS_CODE: number
	LOGIN_EXPIRE: number
	API_URL: string
}

const AdminConfig: Config = {
	BASENAME: '/',
	TOKEN_KEY: 'Admin_Token_key',
	SUCCESS_CODE: 200,
	LOGIN_EXPIRE: 400,
	API_URL: 'https://jsonplaceholder.typicode.com',
}

export default AdminConfig
