import { request } from './request'
import { PageQueryParams, QueryListResponseData } from '../typings'

export interface BlogSearchProps extends PageQueryParams {
	title?: string
	id?: number
}

export interface BlogProps {
	id: number
	title?: string
	desc?: string
}

export function apiGetBlogsMock(params?: BlogSearchProps) {
	return new Promise<QueryListResponseData<BlogProps>>(resolve => {
		setTimeout(() => {
			let fakeList = { data: [{ id: 1, title: 'blog 1', desc: 'desc blog' }] }
			if (params?.title === 'abc') {
				fakeList = {
					data: [{ id: 1, title: 'blog abc', desc: 'desc blog' }],
				}
			}
			resolve(fakeList)
		}, 200)
	})
}

export function apiGetBlogs(params?: BlogSearchProps) {
	return request<QueryListResponseData<BlogProps>>({
		method: 'GET',
		url: '/posts',
		params,
	})
}
