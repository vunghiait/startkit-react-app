import axios, { AxiosRequestConfig, AxiosError, AxiosResponse } from 'axios'
import AdminConfig from '../config'
import { getToken } from '../utils/cookie'
import store from '../store'
import { logout } from '../store/module/user'

interface ResponseData<T> {
	code: number
	data: T
	msg: string
}

axios.defaults.headers = {
	'Content-Type': 'application/json;charset=utf-8',
}

axios.defaults.baseURL = AdminConfig.API_URL

axios.interceptors.request.use(
	(config: AxiosRequestConfig) => {
		const token = getToken()

		if (token) {
			// eslint-disable-next-line no-param-reassign
			config.headers.token = token
		}

		return config
	},
	(error: AxiosError) => Promise.reject(error)
)

axios.interceptors.response.use(
	(response: AxiosResponse<ResponseData<any>>) => {
		if (!response.data) {
			return Promise.resolve(response)
		}

		if (response.data.code === AdminConfig.LOGIN_EXPIRE) {
			if (window.confirm('Login expired!')) {
				store.dispatch(logout())
				window.location.href = `${
					window.location.origin
				}/system/login?redirectURL=${encodeURIComponent(window.location.href)}`
			}
			return Promise.reject(new Error(response.data.msg))
		}

		if (response.data.code === AdminConfig.SUCCESS_CODE) {
			return response.data as any
		}

		console.log(response.data.msg)

		return Promise.reject(new Error(response.data.msg))
	},
	(error: AxiosError) => {
		console.log(error.message)

		return Promise.reject(error)
	}
)

// eslint-disable-next-line import/prefer-default-export
export function request<T>(options: AxiosRequestConfig) {
	return axios.request<T>(options)
}
