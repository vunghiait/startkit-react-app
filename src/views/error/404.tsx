import React from 'react'
import { Link } from 'react-router-dom'

function Error404() {
	return (
		<div>
			<h1>Sorry, the page you visited does not exist.</h1>
			<Link to='/'>Back Home</Link>
		</div>
	)
}

export default Error404
