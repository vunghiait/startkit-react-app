import React from 'react'
import { Link } from 'react-router-dom'

function Error403() {
	return (
		<div>
			<h1>Sorry, you don&apos;t have access to this page.</h1>
			<Link to='/'>Back Home</Link>
		</div>
	)
}

export default Error403
