import React, { useEffect, useState } from 'react'
import { apiGetBlogsMock, BlogProps } from '../api/blog'

function Blog() {
	const [blogs, setBlogs] = useState<BlogProps[]>([])

	const initGetBlogs = async () => {
		try {
			const { data } = await apiGetBlogsMock({ title: 'abc' })
			setBlogs(data)
		} catch (error) {
			// do
		}
	}

	useEffect(() => {
		initGetBlogs()
	}, [])

	return <>{JSON.stringify(blogs)}</>
}

function Welcome() {
	return (
		<>
			<div>Component Welcome</div>
			<h2>Blog</h2>
			<Blog />
		</>
	)
}

export default Welcome
