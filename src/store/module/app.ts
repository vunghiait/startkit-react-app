import {Reducer} from 'redux';
import {IAction} from '../types';

export interface AppState {
  init: boolean;
}

const defaultApp: AppState = {
  init: false,
};

const APP_INITIALIZE = 'APP_INITIALIZE';

export const appInit = (init: AppState['init']) => ({
  type: APP_INITIALIZE,
  payload: init,
});

const appReducer: Reducer<AppState, IAction<any>> = (state = defaultApp, action: IAction<any>) => {
  const {type, payload} = action;

  switch (type) {
    case APP_INITIALIZE:
      return {
        ...state,
        init: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default appReducer;
