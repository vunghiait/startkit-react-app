import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import { systemRoutes } from '../router/utils'
import { IRoute } from '../router/config'

interface UserLayoutState {
	isError: boolean
}

class SystemLayout extends React.PureComponent<any, UserLayoutState> {
	// eslint-disable-next-line react/state-in-constructor
	state: UserLayoutState = {
		isError: false,
	}

	static getDerivedStateFromError() {
		return { isError: true }
	}

	componentDidCatch() {}

	render() {
		if (this.state.isError) {
			return <div>Something wrong...</div>
		}

		return (
			<>
				<Helmet>
					<title>User layout</title>
					<meta name='description' content='desc' />
				</Helmet>
				<div className='container-login'>
					<div className='top'>
						<h1>User layout</h1>
					</div>
					<Suspense fallback={<div>loading...</div>}>
						<Switch>
							{systemRoutes.map((menu: IRoute) => (
								<Route
									exact
									key={menu.path}
									path={menu.path}
									component={menu.component}
								/>
							))}
						</Switch>
					</Suspense>
				</div>
			</>
		)
	}
}

export default SystemLayout
