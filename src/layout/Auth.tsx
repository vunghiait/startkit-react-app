import React, { memo } from 'react'
import { Redirect, RouteComponentProps } from 'react-router-dom'
import { rootRoutes } from '../router/utils'
import { getToken } from '../utils/cookie'
import { IRoute } from '../router/config'
import config from '../config/index'

interface AuthProps extends RouteComponentProps {
	route: IRoute
	children: React.ReactNode
}

function Auth(props: AuthProps) {
	if (props.route.redirect) {
		return <Redirect to={props.route.redirect!} push />
	}

	const route = rootRoutes.find(child => child.path === props.location.pathname)

	if (!route) {
		return <Redirect to='/error/404' push />
	}

	if (route.auth) {
		if (!getToken()) {
			return (
				<Redirect
					to={`/system/login?redirectURL=${encodeURIComponent(
						window.location.origin +
							config.BASENAME +
							props.location.pathname +
							props.location.search
					)}`}
				/>
			)
		}

		// check valid access role
		// return <Redirect to='/error/403' push />
	}

	return <>{props.children}</>
}

export default memo(Auth)
