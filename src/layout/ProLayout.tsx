import React, { Suspense, useMemo } from 'react'
import { Switch } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import Header from './pro/Header'

import AsyncRouters from './AsyncRouters'
import '../styles/index.scss'

function Layout() {
	const routeList = useMemo(() => AsyncRouters(), [])

	return (
		<>
			<Helmet>
				<title>Pro layout</title>
				<meta name='description' content='desc' />
			</Helmet>
			<div id='pro-layout' className='bg-gray'>
				<h1>Pro Layout</h1>
				<Header />
				<Suspense fallback={<div>loading...</div>}>
					<Switch>{routeList}</Switch>
				</Suspense>
			</div>
		</>
	)
}

export default Layout
