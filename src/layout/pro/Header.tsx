import React from 'react'
import { Link } from 'react-router-dom'

function Header() {
	return (
		<ul>
			<li>
				<Link to='/welcome'>Welcome</Link>
			</li>
			<li>
				<Link to='/access'>Access</Link>
			</li>
		</ul>
	)
}

export default Header
