import React from 'react'
import { Route } from 'react-router-dom'

import Auth from './Auth'
import { IRoute } from '../router/config'
import { rootRoutes } from '../router/utils'

// export const MapMenus = routers.filter(route => route.path === '/')[0].children

function renderRoute(route: IRoute) {
	const { component: Component } = route

	return (
		<Route
			key={route.path}
			exact={route.path !== '*'}
			path={route.path}
			render={props => (
				<Auth route={route} {...props}>
					<Component {...props} />
				</Auth>
			)}
		/>
	)
}

function AsyncRouters(): React.ReactNode[] {
	const result: React.ReactNode[] = []

	rootRoutes.forEach((child: IRoute) => {
		result.push(renderRoute(child))
	})

	return result
}

export default AsyncRouters
