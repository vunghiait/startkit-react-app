export interface PageQueryParams {
	page?: number
	size?: number
}

export interface PageResponseData {
	dataTotal?: number
	pageTotal?: number
	page?: number
	size?: number
}

export interface QueryListResponseData<T> {
	data: T[]
	page?: PageResponseData
}
