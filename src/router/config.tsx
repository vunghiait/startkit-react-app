import React from 'react'

export interface IRouteBase {
	path: string
	component?: any
	redirect?: string
	meta?: IRouteMeta
	access?: string[]
	auth?: boolean
	name: string
	icon?: React.ReactNode
}

export interface IRouteMeta {
	name: string
	icon?: React.ReactNode
}

export interface IRoute extends IRouteBase {
	children?: IRoute[]
}

const routes: IRoute[] = [
	{
		path: '/system',
		component: React.lazy(() => import('../layout/SystemLayout')),
		name: 'System',
		redirect: '/system/login',
		children: [
			{
				path: '/system/login',
				component: React.lazy(() => import('../views/system/Login')),
				name: 'Login',
			},
		],
	},
	{
		path: '/',
		component: React.lazy(() => import('../layout/ProLayout')),
		name: 'ProLayout',
		redirect: '/welcome',
		children: [
			{
				path: '/welcome',
				name: 'welcome',
				icon: '',
				component: React.lazy(() => import('../views/Welcome')),
				auth: false,
			},
			{
				path: '/access',
				name: 'access',
				icon: '',
				component: React.lazy(() => import('../views/Welcome')),
			},
			{
				path: '/error',
				redirect: '/error/404',
				name: '404',
				children: [
					{
						path: '/error/404',
						name: 'Not Found',
						component: React.lazy(() => import('../views/error/404')),
						auth: false,
					},
					{
						path: '/error/403',
						name: 'Access Deni',
						component: React.lazy(() => import('../views/error/403')),
						auth: false,
					},
				],
			},
			{
				path: '*',
				name: 'Page',
				redirect: '/error/404',
			},
		],
	},
]
export default routes
