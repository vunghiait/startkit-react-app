import React, { Suspense } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { IRoute } from './router/config'
import { layoutRoutes } from './router/utils'
import config from './config'

function App() {
	return (
		<Suspense fallback={<div>loading...</div>}>
			<Router basename={config.BASENAME}>
				<Switch>
					{layoutRoutes.map((route: IRoute) => (
						<Route
							key={config.BASENAME + route.path}
							path={route.path}
							component={route.component}
						/>
					))}
				</Switch>
			</Router>
		</Suspense>
	)
}

export default App
